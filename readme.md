![DogsGalleryLogo](https://user-images.githubusercontent.com/7823937/128541245-0e3a6b56-586e-447d-adc6-0c4903ce5cfb.png)  
# Dogs Gallery

Chip Financial Home Assignment, Kyung Geun Lee  
4th ~ 6th Aug 2021  

## Introduction
Dogs Gallery is an Android native app that shows a picture of a dog of a breed selected by the user.

## Screenshots
![dogs_gallery1](https://user-images.githubusercontent.com/7823937/128543217-5a7e2981-37f3-4344-b936-d066f5deffa1.jpeg) ![dogs_gallery2](https://user-images.githubusercontent.com/7823937/128543224-da92c416-3f67-4c8e-89a3-57ebecba1133.jpeg) ![dogs_gallery3](https://user-images.githubusercontent.com/7823937/128543230-b13ff629-73ce-4a20-83ac-254ce9c1bd71.jpeg)  


## How to use the app
1. You can download APK file here : https://drive.google.com/drive/folders/127_yJo0bbDrl9SsF3PAfaxG3CBNd1OLU?usp=sharing  
2. Please download and install the DogsGallery_live_release_1.0_1_20210806.apk file.  


## Source Repository
https://bitbucket.org/freebuds/dogs-gallery/src/master/

## Firebase Console
https://console.firebase.google.com/  
You can check the statistics on the app through Firebase. I will deliver the account that can check separately.    

## Tech stacks

- Language : **Kotlin**  
- **MVVM Architecture, LiveData, DataBinding** : The architecture is used for clean code and instant view updates.
- **Room** : Used for data persistnce.
- **Hilt** : For clean architecture application, used Hilt, which is Google's official library and much simpler to use than Dagger.  
- **Navigation Architecture Component** : Chosen to quickly construct multi-step screens. 
- **Glide** : Used for caching images and loading external images, optimal image processing and scalability.  
- **Retrofit** : It was judged to be an efficient form of communication library to be applied to reactive programming and used.
- **Firebase** : Firebase library has been applied to check application indicators such as Crashlytics and Event and for applying additional features in the future  


## Introduction to the project focus
1. It is designed with a similar feel so that it can be thought of as a product family of Chip.
2. applied animations to make the screen transition feel smooth.
3. Separated the flavor for the dev version and the live version, and created an app signing keystore file. (total 4 flavors : devDebug, devRelease, liveDebug, liveRelease)
4. You can see the statistics used by actual users by applying the Firebase. (crashes by Crashlytics, each click and screenview by Events)


## Test Cases and Results (Unit Test through JUnit4)
1. Testing for Networking

> Input : Breeds List  
> Result : **Pass**
      
> Input : Image List  
> Result : **Pass**
      
2. Testing for Room Database and ViewModel
      
> Input : Customer Data  
> Result : **Pass**
      
> Input : Rewards Result  
> Result : **Pass**
      
**Total** : 4 cases  
**Pass**: 4 cases  
**Fail** : 0 cases  
  
## Copyright

All images and resources used in the project were personal or commercially licensed.  
Vector images were downloaded from freepik.com or remixicon.com  