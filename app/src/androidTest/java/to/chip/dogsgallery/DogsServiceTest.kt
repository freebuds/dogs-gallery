package to.chip.dogsgallery

import androidx.lifecycle.viewModelScope
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.launch
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import to.chip.dogsgallery.constants.CDGConstants
import to.chip.dogsgallery.data.persistance.CDGAppDatabase
import to.chip.dogsgallery.data.persistance.CDGMainDao
import to.chip.dogsgallery.network.CDGDogsService
import to.chip.dogsgallery.viewmodel.CDGMainViewModel

class DogsServiceTest {
    private val context = InstrumentationRegistry.getInstrumentation().targetContext
    private lateinit var mainDao: CDGMainDao
    private lateinit var mDatabase: CDGAppDatabase
    private lateinit var mainViewModel: CDGMainViewModel
    private lateinit var dogService: CDGDogsService

    @Before
    fun init() {
        mDatabase = Room.databaseBuilder(
            context,
            CDGAppDatabase::class.java,
            "dogsgallery"
        ).build()
        mainDao = mDatabase.CDGMainDao()
        dogService = Retrofit.Builder()
            .baseUrl(CDGDogsService.DOGS_GALLERY_API_URL)
            .addConverterFactory(
                MoshiConverterFactory.create(
                    Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
                )
            )
            .build()
            .create(CDGDogsService::class.java)

        dogService = Retrofit.Builder()
            .baseUrl(CDGDogsService.DOGS_GALLERY_API_URL)
            .addConverterFactory(
                MoshiConverterFactory.create(
                    Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
                )
            )
            .build()
            .create(CDGDogsService::class.java)

        mainViewModel = CDGMainViewModel(mainDao, dogService)

    }

    @Test
    fun breedsListTest() {
        mainViewModel.viewModelScope.launch {
            try {
                val result = dogService.fetchBreeds()
                if (result.status == CDGConstants.SERVER_SUCCESS) {
                    Assert.assertEquals(CDGConstants.SERVER_SUCCESS, result.status)
                } else {
                    throw InternalException()
                }
            } catch (e: Exception) {
                throw e
            }
        }
    }

    @Test
    fun imageListTest() {
        mainViewModel.viewModelScope.launch {
            try {
                val result = dogService.fetchImagesList("shepherd australian")
                if (result.status == CDGConstants.SERVER_SUCCESS) {
                    Assert.assertEquals(CDGConstants.SERVER_SUCCESS, result.status)
                } else {
                    throw InternalException()
                }
            } catch (e: Exception) {
                throw e
            }
        }
    }

    inner class InternalException : Exception() {
        override fun toString(): String = context.getString(R.string.internal_server_error)
    }
}