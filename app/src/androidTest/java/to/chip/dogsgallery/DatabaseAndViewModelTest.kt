package to.chip.dogsgallery

import androidx.lifecycle.*
import androidx.room.PrimaryKey
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import to.chip.dogsgallery.data.model.ui.CDGBreedItem
import to.chip.dogsgallery.data.model.ui.CDGImageItem
import to.chip.dogsgallery.data.persistance.CDGAppDatabase
import to.chip.dogsgallery.data.persistance.CDGMainDao
import to.chip.dogsgallery.network.CDGDogsService
import to.chip.dogsgallery.viewmodel.CDGMainViewModel

@RunWith(AndroidJUnit4::class)
class DatabaseAndViewModelTest {
    private val context = InstrumentationRegistry.getInstrumentation().targetContext
    private lateinit var mainDao: CDGMainDao
    private lateinit var mDatabase: CDGAppDatabase
    private lateinit var mainViewModel: CDGMainViewModel
    private lateinit var dogService: CDGDogsService

    @Before
    fun init() {
        mDatabase = Room.databaseBuilder(
            context,
            CDGAppDatabase::class.java,
            "dogsgallery"
        ).build()
        mainDao = mDatabase.CDGMainDao()
        dogService = Retrofit.Builder()
            .baseUrl(CDGDogsService.DOGS_GALLERY_API_URL)
            .addConverterFactory(
                MoshiConverterFactory.create(
                    Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
                )
            )
            .build()
            .create(CDGDogsService::class.java)

        mainViewModel = CDGMainViewModel(mainDao, dogService)

        mainViewModel.viewModelScope.launch {
            withContext(Dispatchers.IO) {
                mainDao.truncateBreeds()
                mainDao.truncateImage()
            }
        }
    }

    @Test
    fun breedsTest() {
        mainViewModel.viewModelScope.launch {
            val data = CDGBreedItem(
                0,
                "TEST",
                "test",
                0
            )
            withContext(Dispatchers.IO) {
                mainDao.insertBreed(data)
            }

            // DB, ViewModel Test (livedata)
            mainDao.getBreedsList().observeOnce {
                if (it != null) {
                    assertEquals(data, it[0])
                }
            }
        }
    }

    @Test
    fun imageTest() {
        mainViewModel.viewModelScope.launch {
            val data = CDGImageItem(
                imageId = 1000L,
                breedName = "dogsbreed",
                thumbnailUrl = "test"
            )

            withContext(Dispatchers.IO) {
                mainDao.insertImage(data)
            }

            // DB, ViewModel Test (livedata)
            mainDao.getImagesList("dogsbreed").observeOnce {
                if (it != null && it.isNotEmpty()) {
                    assertEquals(data, it[0])
                }
            }
        }
    }
}

class CustomObserver<T>(private val handler: (T?) -> Unit) : Observer<T>, LifecycleOwner {
    private val lifecycle = LifecycleRegistry(this)
    init {
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    override fun getLifecycle(): Lifecycle = lifecycle

    override fun onChanged(t: T) {
        handler(t)
    }
}

fun <T> LiveData<T>.observeOnce(onChangeHandler: (T?) -> Unit) {
    val observer = CustomObserver(handler = onChangeHandler)
    observe(observer, observer)
}
