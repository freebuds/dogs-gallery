package to.chip.dogsgallery.data.model.network

data class CDGBreed(
    val message: Map<String, List<String>>,
    val status: String
)