package to.chip.dogsgallery.data.model.network

data class CDGImageList(
    var message: MutableList<String>,
    val status: String
)